import time
count=0
def collatzSeries(num):
    global count
    count+=1
    if (num==1):
        res=count
        count=0
        return res
    if(num%2==0):
        return collatzSeries(num/2)
    else:
        return collatzSeries(3*num+1)

def travelMax(below,above):
    print "scan %d ~ %d "%(below,above)
    maxNum,maxLength=0,0
    start=time.time()
    for num in xrange(below,above):
        length=debugPrint(num)
        if (length>maxLength):
            maxNum=num
            maxLength=length
    cost=time.time()-start
    print "time=%d, maxNum=%d ,maxLength=%d"%(cost,maxNum,maxLength)
def debugPrint(num):
    res=collatzSeries(num)
#    print "num:"+str(num)+" length="+str(res)
    return res
#num=14
#print "num:"+str(num)+" length="+str(collatzSeries(num))
for i in xrange(1,10):
    pass
#    travelMax(i*10000,(i+1)*10000)
travelMax(1,10000)

print "process finished"
'''
I assume that the longest chain happens on a big number,
so I divide 1000000 into nine part, 
and find max chain length in each field.
scan 100000 ~200000 
time=3, maxNum=156159 ,maxLength=383
scan 200000 ~300000 
time=3, maxNum=230631 ,maxLength=443
scan 300000 ~400000 
time=3, maxNum=345947 ,maxLength=441
scan 400000 ~500000 
time=3, maxNum=410011 ,maxLength=449
scan 500000 ~600000 
time=3, maxNum=511935 ,maxLength=470
scan 600000 ~700000 
time=3, maxNum=626331 ,maxLength=509
scan 700000 ~800000 
time=3, maxNum=704623 ,maxLength=504
scan 800000 ~900000 
time=3, maxNum=837799 ,maxLength=525
scan 900000 ~ 1000000 
time=4, maxNum=939497 ,maxLength=507
process finished

after that, I don't think a number smaller than 100000 has a longer length
however, I check below:
scan 10000 ~ 20000 
time=0, maxNum=17647 ,maxLength=279
scan 20000 ~ 30000 
time=0, maxNum=26623 ,maxLength=308
scan 30000 ~ 40000 
time=0, maxNum=35655 ,maxLength=324
scan 40000 ~ 50000 
time=0, maxNum=45127 ,maxLength=314
scan 50000 ~ 60000 
time=0, maxNum=52527 ,maxLength=340
scan 60000 ~ 70000 
time=0, maxNum=60975 ,maxLength=335
scan 70000 ~ 80000 
time=0, maxNum=77031 ,maxLength=351
scan 80000 ~ 90000 
time=0, maxNum=87087 ,maxLength=333
scan 90000 ~ 100000 
time=0, maxNum=91463 ,maxLength=333
process finished

and also:
    
scan 1 ~ 10000 
time=0, maxNum=6171 ,maxLength=262
'''