l1 = ['one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen']
l2 = ['twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety']

def number2English(t):
	s = ''
	if t >= 100:
		if t % 100 == 0:
			s = l1[t//100 - 1] + 'hundred'
			return s
		else:
			s = l1[t//100 - 1] + 'hundred' + 'and'
			t = t % 100
	if t<20:
		s += l1[t - 1]
	else:
		s += l2[t//10 - 2]
		if not t % 10 == 0:
			s += l1[t % 10 - 1]
	return s
	
print number2English(900)

ss = ''
for i in xrange(1,1000):
	ss += number2English(i)
ss += 'onethousand'
print len(ss)
