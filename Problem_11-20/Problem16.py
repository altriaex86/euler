def rearrange_digits(l):
	size = len(l)
	if size < 1:
		return
	else:
		for i in xrange(0,size):
			if l[i] > 9:
				if i == len(l)-1:
					l.append(1)  
				else:
					l[i+1]+=1
				l[i] -= 10
digits = [2]
power = 1000
for i in xrange(0,power - 1):
	for j in xrange(0,len(digits)):
		digits[j] = digits[j] * 2
	rearrange_digits(digits)
print sum(digits)
