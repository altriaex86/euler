from math import log10
import math

def toDigits(integer):  
    width = int(math.floor(log10(integer))) + 1
    digits = [0] * width
    for i in xrange(width):
        digits[i] = integer % 10
        integer = int(math.floor(integer / 10))
    return digits

def validate(l):
    for i in range(len(l)-1):
        if l[i] > 9:
            l[i+1] = l[i+1] + int(l[i]/10)
            l[i] = l[i] % 10
    if l[-1] > 9:
        l.append(int(l[-1]/10))
        l[-2] = l[-2] % 10
        
    return l
            

def add(l1,l2):
    l3 = []
    i = 0
    over = 0
    l3 = [l1[i] + l2[i] for i in range(min(len(l1),len(l2)))]
    if len(l1) > len(l2):
        for j in l1[i+1:]:
            digit = j
            l3.append(digit)
    elif len(l1) < len(l2):
        for j in l2[i+1:]:
            digit = j
            l3.append(digit)
    l3 = validate(l3)
    return l3

def multi(l1,l2):
    result = [0]
    for i in range(len(l2)):
        resultOfDigit = [0] * len(l1)
        resultOfDigit[i:] = [l2[i] * j for j in l1]
        result = add(result,validate(resultOfDigit))
    return result

result = [1]
for i in range(1,101):
    result = multi(result,toDigits(i))
print sum(result)
        

        
    
    
    