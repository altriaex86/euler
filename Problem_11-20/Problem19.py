
weekEnum = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun']
monthEnum = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

class Date:
    def __init__(self,year,month,day,week):
        self.y = year
        self.m = monthEnum.index(month)
        self.d = day
        self.w = weekEnum.index(week)
    def __str__(self):
        return "%d-%s-%02d %s"%(self.y,monthEnum[self.m],self.d,weekEnum[self.w])

def isEqual(Date,date_):
    if (Date.y,Date.m,Date.d) == (date_[0],monthEnum.index(date_[1]),date_[2]):
        return True
    else:
        return False
        

endDate = (2000,'Dec',31)
dateList = [Date(1900,'Jan',1,'Mon')]
 
while not isEqual(dateList[-1],endDate):
    leap = False
    if dateList[-1].y % 4 == 0:
        if dateList[-1].y % 100 != 0:
            leap = True
        elif dateList[-1].y % 400 == 0:
            leap = True
    volume = [31,28,31,30,31,30,31,31,30,31,30,31]
    if leap:
        volume[1] = 29
    week = (dateList[-1].w + 1) % 7
    year = dateList[-1].y
    month = dateList[-1].m
    day = dateList[-1].d + 1
    if day > volume[dateList[-1].m]:
        day = 1
        month = month + 1
        if month > 11:
            year = year + 1
            month = month % 12
    month = monthEnum[month]
    week = weekEnum[week]
    newDate = Date(year,month,day,week)
    dateList.append(newDate) 
       
startDate = (1901,'Jan',1)
numOfSunday = 0
started = False
for i in dateList:
    if not isEqual(i,startDate) and not started:
        continue
    started = True
    if i.d == 1 and i.w == 6:
        numOfSunday = numOfSunday + 1
print numOfSunday

