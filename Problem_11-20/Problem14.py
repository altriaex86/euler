import Queue
#use queue to make use of the recursive prospect of this series while reduce the #calling overhead
upperbound = 100
numbers = [1]
counts = [1]
for i in xrange(2,upperbound):
	if i in numbers:
		continue
	else:
		count = 1
		j = i
		while j > 1:
			if j % 2 == 0:
				if j / 2 < i or j / 2 in numbers[i:len(numbers)]:
					count = count + counts[j/2 - 1]
					numbers.append(i)
					counts.append(count)
					break
				else:
					j = j / 2
					count = count + 1
			else:
				if j * 3 + 1 in numbers[i:len(numbers)]: 
					count = count + counts[numbers.index(j * 3 + 1)]
					numbers.append(i)
					counts.append(count)
					break
				else:
					j = j * 3 + 1
					count = count + 1
maximum = max(counts)
#print numbers
#print counts
print numbers[counts.index(maximum)]
print maximum