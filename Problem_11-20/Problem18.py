def getParent(i,j,triangle): #find parents of (i,j), assume parents exist
    if j == 0:
        return (0,triangle[i-1][0])
    elif j == len(triangle[i]) - 1:
        return (triangle[i-1][-1],0)
    else:
        return (triangle[i-1][j-1],triangle[i-1][j])


data = []
with open("Problem18-data.txt",'r') as f:
    lines=f.readlines()
    for line in lines:
        s = line.split(' ')
        column = [int(words) for words in s]
        data.append(column)

for i in xrange(1,len(data)):
    for j in xrange(len(data[i])):
        data[i][j] = data[i][j] + max(getParent(i,j,data))
print max(data[-1])
      