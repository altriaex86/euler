import datetime
index = 1
sum = 0
target = 100000000
start = datetime.datetime.now() # proto type
while index<target:
	if index % 3 == 0:
		sum = sum + index
	elif index % 5 == 0:
		sum = sum + index
	index = index + 1
end = datetime.datetime.now()
print (end - start).total_seconds()
print sum
index = 1
sum = 0
start = datetime.datetime.now() # version 2 revised if-else logic
while index<target:
	if index % 3 == 0 or index % 5 == 0:
		sum = sum + index
	index = index + 1
end = datetime.datetime.now()
print (end - start).total_seconds()
print sum

sum = 0
def SumDivisible(n): # version 3 the method included in document for problem 1
	p = (target - 1 ) // n
	return n * (p * (p + 1)) / 2
start = datetime.datetime.now()
sum =  SumDivisible(3) + SumDivisible(5) - SumDivisible(15)
end = datetime.datetime.now()
#print (end - start).total_seconds()
#print sum
print 'done'