import math
def largestfactor(x):
	factor = int(math.sqrt(x))
	while factor >= 1:
		if x % factor == 0:
			return factor
		else:
			factor = factor - 1
	return factor

target = 600851475143
start = int(math.sqrt(target)) #start frome upper bound
while start>=1:
	if target % start == 0 and largestfactor(start) == 1:
		print 'largest prime factor is'
		print start
		break
	else:
		start = start - 1
print 'done'