import math
def ispalindrome(x):
	listsize = int(math.log10(x)) + 1
	digitlist = [0] * listsize
	index = 1
	y = x
	for i in range (0,listsize):
		digitlist[listsize - 1 - i] = y // math.pow(10, listsize - 1 - i)
		y = y - digitlist[listsize - 1 - i] * math.pow(10, listsize - 1 - i)
	for i in range (0,listsize/2):
		if digitlist[i] != digitlist[listsize - 1 - i]:
			return False
	else:
		return True

maximum = 1;
#for i in range(100,1000):
#	for j in range(100,1000):
#		result = i * j
#		if ispalindrome(result) and result > maximum:
#			maximum = result
#else:
#	print maximum

i = 999
while i >= 100:
	j = 999
	while j >= 100:
		result = i * j
		if ispalindrome(result) and result > maximum:
			maximum = result
		j = j - 1
	i = i - 1
print maximum
		